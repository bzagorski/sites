var gulp = require("gulp");
var babel = require("gulp-babel");

gulp.task("build", function () {
    return gulp.src('./src/main/resources/static/js/main.js')
        .pipe(babel())
        .pipe(gulp.dest('./src/main/resources/static/js/babeled'));
});