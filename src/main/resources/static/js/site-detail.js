const deleteButton = document.querySelector('#site-delete')

deleteButton.addEventListener('click', (e) => {
    const result = window.confirm('Are you sure you want to delete this site?')
    if (result) {
        return true
    } else {
        e.preventDefault()
        return false
    }
})