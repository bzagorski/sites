$(document).ready(function () {
    $(".sidenav").sidenav();
    $('.collapsible').collapsible();
    // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();
    $('select').formSelect();
    $('.fixed-action-btn').floatingActionButton();
    $('.fixed-action-btn > ul .btn-floating').tooltip({
        enterDelay: 300
    });
});

const suggestions = document.querySelector('#suggestions');
const searchInput = document.querySelector('#search');
const searchClose = document.querySelector('#search-close');
const siteFilter = document.querySelector('#filter');
const siteFilterClose = document.querySelector('#filter-close');
const sites = [];

$.getJSON('/data/sites', response => {
    $.each(response, function(index, el) {
        sites.push(el);
    });
});

searchInput.addEventListener('input', displayMatches);
searchClose.addEventListener('click', clearSearchField);

if (siteFilter) {
    siteFilter.addEventListener('keyup', filterSitesList);
    siteFilterClose.addEventListener('click', () => {
        siteFilter.value = '';
        filterSitesList();
    });
}

function findMatches(searchToMatch, sites) {
    return sites.filter(site => {
        // Regex on search term globally and insensitively
        const regex = new RegExp(searchToMatch, 'gi');
        return site.name.match(regex) || site.url.match(regex);
    });
}

function displayMatches() {
    let currentFocus = -1;
    const matchArray = this.value.length > 0 ? findMatches(this.value, sites) : [];

    suggestions.style.display = 'block';
    suggestions.innerHTML = matchArray.map(site => {
        const regex = new RegExp(this.value, 'gi');
        const siteName = site.name.replace(regex, `<span class="hl">${this.value}</span>`);

        return `
            <li>
                <a href="/sites/${site.id}"><span class="name">${siteName} ${getEnvironmentName(site.env)}</span></a>
            </li>
        `;
    }).join('');

    searchInput.addEventListener('keydown', e => {
        let context = suggestions;
        if (context) context = context.querySelectorAll('li > a');

        if (e.keyCode === 40) { // Increase focus when pressing down key
            currentFocus++;
            addActive(context);
        } else if (e.keyCode === 38) { // Decrease focus if pressing up key
            currentFocus--;
            addActive(context);
        } else if (e.keyCode === 9) { // Set input val to name if when pressing tab
            searchInput.value = context[currentFocus].innerText;
            currentFocus = -1; // Set so that enter key event does not trigger
            searchInput.focus();
            suggestions.style.display = 'none';
            //document.getElementById('search-form').submit();
        } else if (e.keyCode === 13) { // Press Enter key and simulate click
            if (currentFocus > -1) {
                e.preventDefault();
                if (context) context[currentFocus].click();
            }
        } else if (e.keyCode === 27) { // Hide result list on ESC press
            currentFocus = -1;
            clearSearchField();
        }
    });

    function addActive(context) {
        if (!context) return false;
        removeActive(context);
        if (currentFocus >= context.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (context.length - 1);
        context[currentFocus].classList.add('typeahead-active');
    }

    function removeActive(context) {
        for (let i = 0; i < context.length; i++) {
            context[i].classList.remove('typeahead-active');
        }
    }

    document.addEventListener('click', e => {
        if (e.target !== suggestions) clearSearchField();
    });
}

function filterSitesList() {
    const filterValue = siteFilter.value;
    const regex = new RegExp(filterValue, 'gi');
    const siteRows = document.querySelectorAll('#sites > tbody > tr');

    siteRows.forEach(siteRow => {
        let siteName = siteRow.getElementsByTagName('td')[0].innerText;
        let siteUrl = siteRow.getElementsByTagName('td')[1].innerText;

        siteName.match(regex) || siteUrl.match(regex)
            ? siteRow.style.display = ''
            : siteRow.style.display = 'none';
    });
}

function clearSearchField() {
    searchInput.value = '';
    suggestions.style.display = 'none';
}

function getEnvironmentName(env) {
    return env === 'DEV' ? 'Dev' : env === 'PROD' ? 'Prod' : '';
}