'use strict';

$(document).ready(function () {
    $(".sidenav").sidenav();
    $('.collapsible').collapsible();
    // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();
    $('select').formSelect();
    $('.fixed-action-btn').floatingActionButton();
    $('.fixed-action-btn > ul .btn-floating').tooltip({
        enterDelay: 300
    });
});

var suggestions = document.querySelector('#suggestions');
var searchInput = document.querySelector('#search');
var searchClose = document.querySelector('#search-close');
var siteFilter = document.querySelector('#filter');
var siteFilterClose = document.querySelector('#filter-close');
var sites = [];

$.getJSON('/data/sites', function (response) {
    $.each(response, function (index, el) {
        sites.push(el);
    });
});

searchInput.addEventListener('input', displayMatches);
searchClose.addEventListener('click', clearSearchField);

if (siteFilter) {
    siteFilter.addEventListener('keyup', filterSitesList);
    siteFilterClose.addEventListener('click', function () {
        siteFilter.value = '';
        filterSitesList();
    });
}

function findMatches(searchToMatch, sites) {
    return sites.filter(function (site) {
        // Regex on search term globally and insensitively
        var regex = new RegExp(searchToMatch, 'gi');
        return site.name.match(regex) || site.url.match(regex);
    });
}

function displayMatches() {
    var _this = this;

    var currentFocus = -1;
    var matchArray = this.value.length > 0 ? findMatches(this.value, sites) : [];

    suggestions.style.display = 'block';
    suggestions.innerHTML = matchArray.map(function (site) {
        var regex = new RegExp(_this.value, 'gi');
        var siteName = site.name.replace(regex, '<span class="hl">' + _this.value + '</span>');

        return '\n            <li>\n                <a href="/sites/' + site.id + '"><span class="name">' + siteName + ' ' + getEnvironmentName(site.env) + '</span></a>\n            </li>\n        ';
    }).join('');

    searchInput.addEventListener('keydown', function (e) {
        var context = suggestions;
        if (context) context = context.querySelectorAll('li > a');

        if (e.keyCode === 40) {
            // Increase focus when pressing down key
            currentFocus++;
            addActive(context);
        } else if (e.keyCode === 38) {
            // Decrease focus if pressing up key
            currentFocus--;
            addActive(context);
        } else if (e.keyCode === 9) {
            // Set input val to name if when pressing tab
            searchInput.value = context[currentFocus].innerText;
            currentFocus = -1; // Set so that enter key event does not trigger
            searchInput.focus();
            suggestions.style.display = 'none';
            //document.getElementById('search-form').submit();
        } else if (e.keyCode === 13) {
            // Press Enter key and simulate click
            if (currentFocus > -1) {
                e.preventDefault();
                if (context) context[currentFocus].click();
            }
        } else if (e.keyCode === 27) {
            // Hide result list on ESC press
            currentFocus = -1;
            clearSearchField();
        }
    });

    function addActive(context) {
        if (!context) return false;
        removeActive(context);
        if (currentFocus >= context.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = context.length - 1;
        context[currentFocus].classList.add('typeahead-active');
    }

    function removeActive(context) {
        for (var i = 0; i < context.length; i++) {
            context[i].classList.remove('typeahead-active');
        }
    }

    document.addEventListener('click', function (e) {
        if (e.target !== suggestions) clearSearchField();
    });
}

function filterSitesList() {
    var filterValue = siteFilter.value;
    var regex = new RegExp(filterValue, 'gi');
    var siteRows = document.querySelectorAll('#sites > tbody > tr');

    siteRows.forEach(function (siteRow) {
        var siteName = siteRow.getElementsByTagName('td')[0].innerText;
        var siteUrl = siteRow.getElementsByTagName('td')[1].innerText;

        siteName.match(regex) || siteUrl.match(regex) ? siteRow.style.display = '' : siteRow.style.display = 'none';
    });
}

function clearSearchField() {
    searchInput.value = '';
    suggestions.style.display = 'none';
}

function getEnvironmentName(env) {
    return env === 'DEV' ? 'Dev' : env === 'PROD' ? 'Prod' : '';
}