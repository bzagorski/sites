<#import "../main.ftl" as main/>

<@main.content>
    <div class="site-add-wrapper">
        <div class="row" id="site-add-header">
            <h4 class="col s12">Add a New Site</h4>
        </div>
        <#include "/includes/site-edit-form.ftl"/>
    </div>
</@main.content>