<#import "../main.ftl" as main>

<@main.content>
    <div class="platform-header row">
<#--          <div class="col s6 input-field">
            <img class="platform-logo" src="/img/${platform.code}_mark.png" alt="${platform.name}">
        </div>  -->
        <h4>${platform.name}</h4>
        <div class="input-field col s6 right">
            <input id="filter" type="search" required>
            <label for="filter">Table Filter</label>
            <i class="material-icons search-close" id="filter-close">close</i>
        </div>
    </div>
    <div class="platform-table row">
        <#include "/includes/site-table.ftl"/>
    </div>
</@main.content>