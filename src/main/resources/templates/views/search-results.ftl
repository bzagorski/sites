<#import "../main.ftl" as main>

<@main.content>
    <h5 class="search-header">Results:</h5>
    <br>
    <#include "/includes/site-table.ftl"/>
</@main.content>