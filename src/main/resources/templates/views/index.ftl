<#import "../main.ftl" as main/>

<@main.content>
    <ul class="collapsible popout" id="platforms" data-collapsible="accordion">
    <#list platformSiteMap as key, siteMap>
        <li class="platform-collapsible">
            <div class="collapsible-header"><i class="tiny material-icons">more_vert</i>${key.name}</div>
            <div class="collapsible-body">
                    <span>
                        <#include "/includes/site-table.ftl"/>
                    </span>
            </div>
        </li>
    </#list>
    </ul>
</@main.content>