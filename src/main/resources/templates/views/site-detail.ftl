<#import "../main.ftl" as main/>
<@main.content>
    <div class="row" id="site-details-wrapper">
        <div class="container">
            <div class="col s12">
                <div class="card">
                    <div class="card-image waves-effect waves-block waves-light">
                        <a class="col platform-logo-container" href="/platforms/${site.platform.code}/sites">
                            <img class="platform-logo" id="card-site-logo" src="/img/${site.platform.code}_mark.png" alt="${site.platform.code}"/>
                        </a>
                    </div>
                    <div class="card-content white-text ${site.platform.code}">
                        <span class="card-title">${site.name}</span>
                        <ul id="site-details">
                            <li>Site Platform: ${site.platform.name}</li>
                            <li>Environment: ${site.env}</li>
                            <li>Site Url: <a href="${site.url}" target="_blank">${site.url}</a></li>
                        </ul>
                    </div>
                    <div class="card-action">
                        <a id="site-go" href="${site.url}" target="_blank">Go</a>
                        <a id="site-edit" class="modal-trigger" data-target="edit-site">Edit</a>
                        <a id="site-delete" href="/sites/${site.id?c}/delete">Delete</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Structure -->
        <div id="edit-site" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h5>Edit Site: ${site.name}</h5>
            <#include "/includes/site-edit-form.ftl"/>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/js/site-detail.js"></script>

</@main.content>