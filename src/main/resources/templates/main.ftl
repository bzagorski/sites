<#macro content>
    <#include "/includes/header.ftl"/>
    <body>
    <main id="main" class="container">
        <div class="fixed-action-btn" id="menu">
            <a id="btn-menu" class="btn-floating btn-large waves-effect waves-light red">
                <i class="large material-icons">menu</i>
            </a>
            <ul>
                <li><a id="add" class="btn-floating cyan" href="/sites/add" data-position="left" data-tooltip="Add a new site">
                    <i class="material-icons">add</i></a>
                </li>
                <li><a id="export" class="btn-floating green" href="/sites/exportAll" data-position="left" data-tooltip="Export all sites">
                    <i class="material-icons">file_download</i></a>
                </li>
            </ul>
        </div>
    <#if flashMessage??>
        <div class="row">
            <div class="col s12 m5">
                <div id="flash-message" class="card-panel cyan">
                    <span class="white-text">${flashMessage}</span>
                </div>
            </div>
        </div>
    </#if>

    <#nested> <!-- Adds content from other template -->

    <#include "/includes/footer.ftl"/>
</#macro>

