<#import "main.ftl" as main>

<@main.content>
<div class="container">
    <div class="row">
        <div class="col s12">
            <div id="notFoundContent" class="section">
                <h2 class="header">Looks like something went wrong!</h2>
                <#if exception??>

                    <blockquote id="exception-message">${exception}</blockquote>
                </#if>
                <p>
                    The page you were looking for does not exist. If you think this is a mistake throw me an <a
                        href="mailto:bzagorski@demandbridge.com">email. </a>
                    Click below to return to the homepage
                </p>
                <a href="/" class="btn-large floating waves-effect waves-light cyan">Return to Homepage</a>
            </div>
        </div>
    </div>
</div>
</@main.content>