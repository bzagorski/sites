<nav>
    <div class="nav-wrapper grey">
        <a href="/" class="brand-logo"><img src="/img/dblogo.svg" alt="db" width="179px"></a>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
            <li>
                <form action="/sites/search" method="get" id="search-form">
                    <div class="typeahead input-field">
                        <input name="q" id="search" type="search" autocomplete="off" required>
                        <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                        <i class="material-icons" id="search-close">close</i>
                        <ul id="suggestions">

                        </ul>
                    </div>
                </form>
            </li>
            <#list platforms as platform>
                <li class="nav-link"><a href="/platforms/${platform.id}/sites">${platform.name}</a></li>
            </#list>
        </ul>
    </div>
</nav>

<ul class="sidenav" id="nav-mobile">
    <li><a href="/">Home</a></li>
    <#list platforms as platform>
        <li class="nav-link"><a href="/platforms/${platform.id}/sites">${platform.name}</a></li>
    </#list>
    <li><a href="/sites/add">Add a Site</a></li>
    <li><a href="/sites/exportAll">Export Sites</a></li>
</ul>