<div class="row">
    <form id="siteForm" class="col s12" action=<#if site??>"/sites/${site.id?c}/edit"<#else>"/sites/add"</#if> method="post">
        <div class="row">
            <div class="input-field col s6">
                <input id="site-name" type="text" name="name" class="validate" <#if site??>value="${site.name}"</#if>>
                <label for="site-name">Site Name</label>
            </div>
            <div class="input-field col s6">
                <input id="site-url" type="text" name="url" class="validate" <#if site??>value="${site.url}"</#if>>
                <label for="site-url">Site Url</label>
            </div>
        </div>
        <div class="row">
            <div id="site-env" class="col s6">
                <label>Environment</label>
                <p>
                    <label>
                        <input name="env" type="radio" id="env-dev"
                               value="DEV" <#if site??><#if site.env.ordinal() = 0>checked</#if></#if> />
                        <span>Dev</span>
                    </label>
                </p>
                <p>
                    <label>
                        <input name="env" type="radio" id="env-prod"
                               value="PROD" <#if site??><#if site.env.ordinal() = 1>checked</#if></#if> />
                        <span>Prod</span>
                    </label>
                </p>
                <p>
                    <label>
                        <input name="env" type="radio" id="env-other"
                               value="INTERNAL" <#if site??><#if site.env.ordinal() = 2>checked</#if></#if> />
                        <span>N/A</span>
                    </label>
                </p>
            </div>
            <label for="site-platform">Select Platform</label>
            <!-- Remove .browser-default and re-enable initialization in main.js for fancy styling-->
            <div class="input-field col s6">
                <select name="platform" id="site-platform">
                    <option value="" disabled <#if site??><#else>selected</#if>>Choose your option</option>
                    <#list platforms as platform>
                        <option value="${platform.id}" <#if site??><#if platform.id == site.platform.id>selected="selected"</#if></#if>>${platform.name}</option>
                    </#list>
                </select>
            </div>
        </div>
        <div class="col s6 offset-s6">
            <button class="btn waves-effect waves-light cyan" type="submit" name="action" id="save">Submit
                <i class="material-icons right">send</i>
            </button>
        </div>
    </form>
</div>