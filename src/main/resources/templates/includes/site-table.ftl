    <#list siteMap as key, sites>
    <h5>${key.value}</h5>
    <table id="sites">
        <thead>
        <tr>
            <th>Name</th>
            <th>URL</th>
            <th>View Details</th>
        </tr>
        </thead>
        <tbody>
        <#list sites as site>
            <tr>
                <td>${site.name}</td>
                <td><a href="${site.url}" target="_blank">${site.url}</a></td>
                <td><a href="/sites/${site.id?c}"><i class="material-icons">details</i></a></td>
            </tr>
        </tbody>
        </#list>
    </table>
    <br>
    </#list>
