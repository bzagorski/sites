</main> <!-- End page container-->

    <footer class="center">
        <div class="container">
            © 2018 DemandBridge
        </div>
        <span></span>
    </footer>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="/js/materialize.js"></script>
    <script type="text/javascript" src="/js/babeled/main.js"></script>
    </body>
</html>