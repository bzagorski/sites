package com.demandbridge.sites.site

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class SiteRestController {

    final SiteRepository siteRepository

    @Autowired
    SiteRestController(SiteRepository siteRepository) {
        this.siteRepository = siteRepository
    }

    @GetMapping(value = '/data/sites', produces = MediaType.APPLICATION_JSON_VALUE)
    def getAllSites() {
        siteRepository.findAll()
    }
}
