package com.demandbridge.sites.site

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = 'Site does not exist')
class SiteNotFoundException extends Exception {
    SiteNotFoundException(GString message) {
        super(message)
    }

    SiteNotFoundException(GString message, Throwable cause) {
        super(message, cause)
    }
}
