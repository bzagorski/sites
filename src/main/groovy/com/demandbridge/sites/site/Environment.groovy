package com.demandbridge.sites.site

enum Environment {
    DEV('Development'), PROD('Production'), INTERNAL('Internal')

    String value

    Environment(String value) {
        this.value = value
    }

    static Environment fromString(String string) {
        return valueOf(string.toUpperCase())
    }
}