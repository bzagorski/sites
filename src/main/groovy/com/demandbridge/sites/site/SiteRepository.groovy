package com.demandbridge.sites.site

import com.demandbridge.sites.platform.Platform
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface SiteRepository extends JpaRepository<Site, Long> {
    Page<Site> findAll(Pageable pageable)

    List<Site> findAllByOrderByName()

    List<Site> findAllByPlatformOrderByName(Platform platform)

    List<Site> findAllByNameOrUrlContainingAllIgnoreCase(String name, String url)

    // TODO: use optional
    Site findOneByUrl(String url)
}
