package com.demandbridge.sites.site

import com.demandbridge.sites.platform.PlatformNotFoundException
import com.demandbridge.sites.platform.PlatformRepository
import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import org.springframework.web.util.NestedServletException

import javax.servlet.http.HttpServletResponse

@Slf4j
@Controller
@CompileStatic
class SiteController {

    SiteRepository siteRepository
    PlatformRepository platformRepository

    @Autowired
    SiteController(SiteRepository siteRepository, PlatformRepository platformRepository) {
        this.siteRepository = siteRepository
        this.platformRepository = platformRepository
    }

    @ExceptionHandler(SiteNotFoundException)
    def handleSiteNotFound(Model model) {
        log.info('>>> Intercepted SiteNotFoundException')
        model.addAttribute('exception', 'The site you are looking for does not exist')
        return 'error'
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(NestedServletException)
    def errorPage(Exception ex, Model model) {
        model.addAttribute("exception", ex)
        return 'error'
    }

    @CompileStatic(value = TypeCheckingMode.SKIP)
    @GetMapping(['', '/'])
    def home(Model model) {
        def map = siteRepository
                .findAllByOrderByName()
                .groupBy({ it.platform }, { it.env })
                .sort { it.key }
        model.addAttribute("platformSiteMap", map)
        return 'views/index'
    }

    @GetMapping('/sites/{id}')
    def siteDetail(@PathVariable Long id, Model model) {
        // TODO: update with optional pattern
        Site site = siteRepository.findById(id)
                .orElseThrow({ new SiteNotFoundException("Site with id [${id}] was not found") })
        model.addAllAttributes([
                site: site, // todo: update all optionals
                platforms: platformRepository.findAll()
        ])
        return 'views/site-detail'
    }

    @GetMapping('/sites/add')
    def siteForm(Model model) {
        model.addAttribute('platforms', platformRepository.findAll())
        return 'views/site-add'
    }

    @PostMapping('/sites/add')
    def addSite(Site site,
                @RequestParam(name = 'platform', required = false) Long platformId,
                RedirectAttributes redirectAttributes) {
        if (site.name.isEmpty() || site.url.isEmpty() || platformId == null) {
            redirectAttributes.addFlashAttribute('flashMessage', 'Please ensure that all fields are completed.')
            return 'redirect:/sites/add'
        }
        if (siteRepository.findOneByUrl(site.url)) {
            redirectAttributes.addFlashAttribute('flashMessage', 'Site already exists!')
            return 'redirect:/sites/add'
        }
        site.setPlatform(platformRepository.findById(platformId)
                .orElseThrow({ -> new PlatformNotFoundException("No platform found with that id: $platformId") }))
        siteRepository.save(site)
        redirectAttributes.addFlashAttribute('flashMessage', 'Site successfully added')
        return 'redirect:/'
    }

    @PostMapping('/sites/{siteId}/edit')
    def editSite(Site site,
                 @PathVariable Long siteId,
                 @RequestParam(name = 'platform') Long platformId,
                 RedirectAttributes redirectAttributes) {
        if (platformId == null) {
            redirectAttributes.addFlashAttribute('Please select a platform.')
            return "redirect:/sites/$siteId"
        }
        if (site.name.isEmpty() || site.url.isEmpty()) {
            redirectAttributes.addFlashAttribute('flashMessage', 'Please ensure that all fields are completed.')
            return "redirect:/sites/$siteId"
        }
        site.setId(siteId)
        site.setPlatform(platformRepository.findById(platformId)
                .orElseThrow({ -> new PlatformNotFoundException("No platform found with that id: $platformId") }))
        siteRepository.save(site)
        redirectAttributes.addFlashAttribute('flashMessage', 'Site successfully updated')
        return "redirect:/sites/$site.id"
    }

    @GetMapping('/sites/{id}/delete')
    def deleteSite(@PathVariable Long id, RedirectAttributes redirectAttributes) {
        siteRepository.deleteById(id)
        redirectAttributes.addFlashAttribute('flashMessage', 'Site successfully deleted')
        log.info("Deleted site: {}", id)
        return "redirect:/"
    }

    @GetMapping('/sites/search')
    def siteSearchResults(@RequestParam(required = false) String q, Model model) {
        if (q) {
            def results = siteRepository.findAllByNameOrUrlContainingAllIgnoreCase(q, q)
                    .groupBy { it.env }
            model.addAttribute('siteMap', results)
        } else {
            model.addAttribute('siteMap', siteRepository.findAll().groupBy { it.env })
        }
        return 'views/search-results'
    }

    @GetMapping(value = '/sites/exportAll', produces = "text/csv")
    def exportSitesAsCsv(HttpServletResponse response) {
        log.info('Site Export commenced')

        def sites = siteRepository.findAll()
        def responseContent = new StringBuilder()
        def header = 'Name,Url,Environment,Platform\n'

        responseContent.append(header)
        sites.each { responseContent.append("$it.name,$it.url,${it.env.name()},$it.platform.name\n") }

        response.setHeader('Content-Disposition', 'attachment; filename="sites-export.csv"')
        response.getWriter().write(responseContent.toString())
    }

    @GetMapping('/sites/pageDemo')
    def sitesDisplayedAsPages(Model model, Pageable pageable) {
        model.addAllAttributes([
                currentPage: pageable.pageNumber + 1,
                pageSize: pageable.pageSize,
                sites: siteRepository.findAll(pageable).getContent()
        ])
        return 'views/search-results'
    }
}