package com.demandbridge.sites.site

import com.demandbridge.sites.platform.Platform
import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import groovy.transform.ToString
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener

import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import java.time.LocalDateTime

@Entity
@Table(name = 'SITE_LIBRARY_SITE')
@EntityListeners(AuditingEntityListener)
@JsonIgnoreProperties(value = ['createdDate', 'lastModifiedDate'], allowGetters = true)
@ToString
class Site {

    @Id
    @SequenceGenerator(name = 'siteLibrarySiteSeq', sequenceName = 'site_library_site_seq', allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = 'siteLibrarySiteSeq')
    long id

    @NotBlank
    String name

    @Enumerated(EnumType.STRING)
    Environment env

    @NotBlank
    String url

    @NotNull
    @ManyToOne
    @JsonBackReference // Tells jackson to stop here. Prevents infinite recursion
    Platform platform

    @CreatedDate
    @Column(name = 'created_date', nullable = false, updatable = false)
    LocalDateTime createdDate

    @LastModifiedDate
    @Column(name = 'last_modified_date')
    LocalDateTime lastModifiedDate

    String getPlatformCode() {
        return platform.getCode()
    }
}
