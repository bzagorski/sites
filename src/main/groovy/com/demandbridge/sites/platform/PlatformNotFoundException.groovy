package com.demandbridge.sites.platform

class PlatformNotFoundException extends Exception {
    PlatformNotFoundException(GString message) {
        super(message)
    }

    PlatformNotFoundException(GString message, Throwable cause) {
        super(message, cause)
    }
}
