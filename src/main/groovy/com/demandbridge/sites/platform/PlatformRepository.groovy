package com.demandbridge.sites.platform

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PlatformRepository extends JpaRepository<Platform, Long> {
    Optional<Platform> findByCode(String code)
}
