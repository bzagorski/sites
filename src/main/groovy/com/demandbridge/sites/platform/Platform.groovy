package com.demandbridge.sites.platform

import com.demandbridge.sites.site.Site
import com.fasterxml.jackson.annotation.JsonManagedReference

import javax.persistence.*

@Entity
@Table(name = 'SITE_LIBRARY_PLATFORM')
class Platform implements Comparable<Platform> {

    @Id
    @SequenceGenerator(name = 'siteLibraryPlatformSeq', sequenceName = 'site_library_platform_seq', allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = 'siteLibraryPlatformSeq')
    final long id

    String name

    String code

    @OneToMany(mappedBy = "platform")
    @JsonManagedReference
    List<Site> sites = new ArrayList<>()

    @Override
    int compareTo(Platform other) {
        return Long.compare(this.id, other.id)
    }
}