package com.demandbridge.sites.platform

import com.demandbridge.sites.site.SiteRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PathVariable

@Controller
@ControllerAdvice
class PlatformController {

    PlatformRepository platformRepository
    SiteRepository siteRepository

    @Autowired
    PlatformController(PlatformRepository platformRepository, SiteRepository siteRepository) {
        this.platformRepository = platformRepository
        this.siteRepository = siteRepository
    }

    @GetMapping('/platforms/{id}/sites')
    def platformSites(@PathVariable Long id, Model model) {
        Optional<Platform> platform = platformRepository.findById(id)

        if (platform.isPresent()) {
            model.addAllAttributes([
                    platform: platform.get(),
                    siteMap : siteRepository
                            .findAllByPlatformOrderByName(platform.get())
                            .groupBy { it.env }
            ])
            return 'views/platform'
        } else {
            model.addAttribute('exception', 'The platform you are looking for does not exist')
            return 'error'
        }
    }

    @ModelAttribute
    def addPlatformAttributes(Model model) {
        model.addAttribute("platforms", platformRepository.findAll())
    }
}
