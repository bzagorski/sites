package com.demandbridge.sites.config

import oracle.jdbc.pool.OracleDataSource
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaAuditing

import javax.sql.DataSource
import java.sql.SQLException

@Configuration
@EnableJpaAuditing
class PersistenceConfig {

    @Value('${spring.datasource.username}')
    String username

    @Value('${spring.datasource.password}')
    String password

    @Value('${spring.datasource.url}')
    String url

    @Bean
    DataSource dataSource() throws SQLException {
        OracleDataSource dataSource = new OracleDataSource()
        dataSource.setUser(username)
        dataSource.setPassword(password)
        dataSource.setURL(url)
        dataSource.setImplicitCachingEnabled(true)
        dataSource.setFastConnectionFailoverEnabled(true)
        return dataSource
    }
}
