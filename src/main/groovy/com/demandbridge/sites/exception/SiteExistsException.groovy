package com.demandbridge.sites.exception


class SiteExistsException extends Exception {

    final Exception originalException

    public SiteExistsException(Exception originalException, String msg) {
        super(msg)
        this.originalException = originalException
    }
}
