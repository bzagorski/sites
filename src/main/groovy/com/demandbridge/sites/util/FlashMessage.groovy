package com.demandbridge.sites.util


class FlashMessage {
    String message
    Status status

    static enum Status {
        SUCCESS, FAILURE
    }
}
