package com.demandbridge.sites.api

import com.demandbridge.sites.platform.PlatformRepository
import com.demandbridge.sites.site.SiteRepository
import groovy.json.JsonOutput
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping('/api')
class ApiController {

    private final PlatformRepository platformRepository
    private final SiteRepository siteRepository

    @Autowired
    ApiController(PlatformRepository platformRepository, SiteRepository siteRepository) {
        this.platformRepository = platformRepository
        this.siteRepository = siteRepository
    }

    @GetMapping('/sites')
    def getSitesAsJson() {
        return siteRepository.findAll().sort { it.platform.id }
    }

    @GetMapping(path = '/sites/count', produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    def getSiteCount() {
        def siteCounts = platformRepository.findAll()
            .collect { [platform: it.name, siteCount: it.sites.size()] }
        def map = [platformCounts: siteCounts, total: siteRepository.count() ]
        JsonOutput.toJson(map)
    }

    @GetMapping('/platforms')
    def getPlatformsAsJson() {
        return platformRepository.findAll()
    }

    @GetMapping(path = '/platforms/{code}', produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    def platformSitesAsJson(@PathVariable String code) {
        def platform = platformRepository.findByCode(code)

        return platform
                .isPresent() ? platform.get() : JsonOutput.toJson(message: "No platform found with code: $code")
    }
}
