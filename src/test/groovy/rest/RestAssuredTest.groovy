package rest

import com.demandbridge.sites.Application
import com.demandbridge.sites.site.Site
import groovy.json.JsonSlurper
import io.restassured.RestAssured
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import spock.lang.Specification

@SpringBootTest(classes = Application, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase
class RestAssuredTest extends Specification {

    @LocalServerPort
    def port

    def uri

    def setup() {
        uri = "http://localhost:${port}"
    }

    def "Get Sites"() {
        when:
        def result = RestAssured.get("${uri}/data/sites")
        def json = new JsonSlurper().parseText(result.body().print())

        List<Site> sites = [*json]

        then:
        sites instanceof List<Site>
        result.statusCode() == 200
        result.contentType() == 'application/json;charset=UTF-8'
        json.name.any { it == 'DBE Site Prod' }
    }

}
