package com.demandbridge.sites

import com.demandbridge.sites.platform.PlatformRepository
import com.demandbridge.sites.site.SiteRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification

@DataJpaTest
@ActiveProfiles('test')
class DatabaseSpecification extends Specification {

    @Autowired
    PlatformRepository platformRepository

    @Autowired
    SiteRepository siteRepository
}