package com.demandbridge.sites.util

import com.demandbridge.sites.platform.Platform
import com.demandbridge.sites.site.Environment
import com.demandbridge.sites.site.Site

class TestDataProvider {

    static def dbe = new Platform(name: 'DB Enterprise', code: 'dbe')
    static def dba = new Platform(name: 'DB Alliance', code: 'dba')
    static def pay = new Platform(name: 'Pay App', code: 'pay')
    static def cm = new Platform(name: 'Campaign Manager', code: 'cm')
    static def internal = new Platform(name: 'Internal', code: 'itl')
    static def platforms = [dbe, dba, pay, cm, internal]

    static def getTestSites() {
        return [
                new Site(name: 'DemandBridge Demo', platform: dbe, env: Environment.PROD, url: 'https://demandbridge.dbenterprise.com'),
                new Site(name: 'DemandBridge Demo', platform: dbe, env: Environment.DEV, url: 'https://demandbridge.development.dbenterprise.com'),
                new Site(name: 'Automation Nation', platform: dbe, env: Environment.DEV, url: 'https://automation.development.dbenterprise.com'),
                new Site(name: 'Automation Nation', platform: dbe, env: Environment.PROD, url: 'https://automation.mymarketingbench.com'),
                new Site(name: 'Hello Kitty Kitty', platform: dbe, env: Environment.PROD, url: 'https://herekittykitty.mymarketingbench.com'),
                new Site(name: 'ZagTest', platform: dbe, env: Environment.PROD, url: 'https://zagtest.mymarketingbench.com'),
                new Site(name: 'The Dark Side', platform: dbe, env: Environment.PROD, url: 'https://thedarkside.dbenterprise.com'),
                new Site(name: 'SourcingBench', platform: dba, env: Environment.PROD, url: 'https://webbmason.dbenterprise.com/dbvendor'),
                new Site(name: 'SourcingBench Dev', platform: dba, env: Environment.DEV, url: 'https://webbmason.development.dbenterprise.com/dbvendor'),
                new Site(name: 'WM Pay', platform: pay, env: Environment.PROD, url: 'https://webbmason.mymarketingbench.com/pay'),
                new Site(name: 'WM Pay Dev', platform: pay, env: Environment.DEV, url: 'https://webbmason.development.dbenterprise.com/pay'),
                new Site(name: 'DB Demo CM', platform: cm, env: Environment.PROD, url: 'https://dbdemo.dbenterprise.com/cm'),
                new Site(name: 'DB Demo CM Dev', platform: cm, env: Environment.DEV, url: 'https://dbdemo.development.dbenterprise.com/cm'),
                new Site(name: 'DB JIRA', platform: internal, env: Environment.INTERNAL, url: 'https://service.demandbridge.com')
        ]
    }

    static Platform getPlatform(String platformCode) {
        return platforms.find { it.code == platformCode }
    }
}
