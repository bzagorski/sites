package com.demandbridge.sites.api

import com.demandbridge.sites.platform.Platform
import com.demandbridge.sites.platform.PlatformRepository
import com.demandbridge.sites.site.SiteRepository
import com.demandbridge.sites.util.TestDataProvider
import groovy.json.JsonSlurper
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

class ApiControllerSpec extends Specification {

    def siteRepository = Mock(SiteRepository)
    def platformRepository = Mock(PlatformRepository)
    def apiController = new ApiController(platformRepository, siteRepository)
    def mockMvc = MockMvcBuilders.standaloneSetup(apiController).build()

    def "Sites json list"() {
        given:
        siteRepository.findAll() >> TestDataProvider.testSites
        def result = mockMvc.perform(get('/api/sites')).andReturn()
        def json = new JsonSlurper().parseText(result.response.contentAsString)

        expect:
        result.response.contentType == MediaType.APPLICATION_JSON_UTF8_VALUE
        json.url.any { it == 'https://demandbridge.development.dbenterprise.com' }
    }

    def "Sites count"() {
        given:
        siteRepository.count() >> TestDataProvider.testSites.size()
        platformRepository.findAll() >> TestDataProvider.platforms
        def result = mockMvc.perform(get('/api/sites/count')).andReturn()
        def json = new JsonSlurper().parseText(result.response.contentAsString)

        expect:
        result.response.contentType == MediaType.APPLICATION_JSON_UTF8_VALUE
        json.total == TestDataProvider.testSites.size()
    }

    def "Platforms json list"() {
        given:
        platformRepository.findAll() >> TestDataProvider.platforms
        def result = mockMvc.perform(get('/api/platforms')).andReturn()
        def json = new JsonSlurper().parseText(result.response.contentAsString)
        def platforms = [*json] as List<Platform>

        expect:
        platforms.size() > 0
        platforms.any { it.code == 'dbe' }
    }
}
