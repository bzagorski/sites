package com.demandbridge.sites.platform

import com.demandbridge.sites.DatabaseSpecification

class PlatformRepositorySpec extends DatabaseSpecification {

    def "Find All Platforms"() {
        when: "The platformRepository findAll method is called"
        List<Platform> platforms = platformRepository.findAll()

        then: "There should be 5 platforms found"
        assert platforms.size() == 5
    }

    def "Find Platform by Code"() {
        expect: "Platform to be found"
        def platform = platformRepository.findByCode(code)
        platform.isPresent()
        assert platform.get() != null
        assert platform.get().name == name

        where: "Platform code equals"
        code  | name
        'dbe' | 'DB Enterprise'
        'dba' | 'DB Alliance'
        'pay' | 'Pay App'
        'cm'  | 'Campaign Manager'
        'itl' | 'Internal'
    }
}
