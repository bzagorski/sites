package com.demandbridge.sites.platform

import com.demandbridge.sites.site.Environment
import com.demandbridge.sites.site.SiteRepository
import com.demandbridge.sites.util.TestDataProvider
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.PendingFeature
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

class PlatformControllerSpec extends Specification {

    def siteRepository = Mock(SiteRepository)
    def platformRepository = Mock(PlatformRepository)
    def platformController = new PlatformController(platformRepository, siteRepository)
    def mockMvc = MockMvcBuilders.standaloneSetup(platformController).build()

    def platforms = TestDataProvider.platforms
    def sites = TestDataProvider.getTestSites()

    def "View valid platform page"() {
        given:
        def platform = Optional.of(platforms.find { it.code == 'dbe' })
        platformRepository.findById(1) >> platform
        siteRepository.findAllByPlatformOrderByName(platform.get()) >> sites.findAll { it.platform.code == platform.get().code }

        when:
        def result = mockMvc.perform(get("/platforms/1/sites")).andReturn()

        then:
        result.modelAndView.viewName == 'views/platform'
        result.modelAndView.modelMap.platform == platform.get()
        result.modelAndView.modelMap.siteMap.containsKey(Environment.PROD)
        result.modelAndView.modelMap.siteMap.containsKey(Environment.DEV)
        result.modelAndView.modelMap.siteMap.values().size() > 0
        result.modelAndView.modelMap.siteMap.get(Environment.DEV).every { it.platform.code == 'dbe' }
    }

    @PendingFeature
    def "Error for platform that does not exist"() {
        given:
        platformRepository.findByCode(_) >> Optional.empty()

        when:
        def result = mockMvc.perform(get('/platforms/doesNotExist/sites')).andReturn()

        then:
        result.modelAndView.modelMap.exception == 'The platform you are looking for does not exist'
    }
}
