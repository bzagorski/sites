package com.demandbridge.sites.site

import com.demandbridge.sites.platform.Platform
import groovy.json.JsonSlurper
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

class SiteRestControllerWithoutSpringContextSpec extends Specification {

    def siteRepo = Mock(SiteRepository)
    def underTest = new SiteRestController(siteRepo)
    def mockMvc = MockMvcBuilders.standaloneSetup(underTest).build()

    def "Sites are returned"() {
        given:
        def platform = new Platform(name: "DB Enterprise", code: 'dbe')
        def sites = [
                new Site(id: 1, name: 'Test Site 1', url: 'http://test-site-1.demandbridge.com', env: Environment.DEV, platform: platform),
                new Site(id: 2, name: 'Test Site 2', url: 'http://test-site-2.demandbridge.com', env: Environment.PROD, platform: platform),
                new Site(id: 3, name: 'Test Site 3', url: 'http://test-site-3.demandbridge.com', env: Environment.INTERNAL, platform: platform),
                new Site(id: 4, name: 'Test Site 4', url: 'http://test-site-4.demandbridge.com', env: Environment.DEV, platform: platform),
                new Site(id: 5, name: 'Test Site 5', url: 'http://test-site-5.demandbridge.com', env: Environment.DEV, platform: platform)
        ]

        when:
        def response = mockMvc.perform(MockMvcRequestBuilders.get('/data/sites')).andReturn().response
        def json = new JsonSlurper().parseText(response.contentAsString)

        then:
        1 * siteRepo.findAll() >> sites
        json.name.size() == 5
    }
}
