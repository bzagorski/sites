package com.demandbridge.sites.site

import com.demandbridge.sites.Application
import com.demandbridge.sites.MockConfig
import com.demandbridge.sites.platform.Platform
import com.demandbridge.sites.platform.PlatformRepository
import groovy.json.JsonSlurper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

@WebMvcTest(controllers = SiteRestController)
@ContextConfiguration(classes = Application)
@Import(MockConfig)
class SiteRestControllerSpec extends Specification {

    @Autowired
    MockMvc mockMvc

    @Autowired
    SiteRepository siteRepository

    @Autowired
    PlatformRepository platformRepository

    def "Should get a list of sites for site data api"() {
        given: "Site Repository will return a list of 5 sites"
        def platform = new Platform(name: "DB Enterprise", code: 'dbe')
        def sites = [
                new Site(id: 1, name: 'Test Site 1', url: 'http://test-site-1.demandbridge.com', env: Environment.DEV, platform: platform),
                new Site(id: 2, name: 'Test Site 2', url: 'http://test-site-2.demandbridge.com', env: Environment.PROD, platform: platform),
                new Site(id: 3, name: 'Test Site 3', url: 'http://test-site-3.demandbridge.com', env: Environment.INTERNAL, platform: platform),
                new Site(id: 4, name: 'Test Site 4', url: 'http://test-site-4.demandbridge.com', env: Environment.DEV, platform: platform),
                new Site(id: 5, name: 'Test Site 5', url: 'http://test-site-5.demandbridge.com', env: Environment.DEV, platform: platform)
        ]
        siteRepository.findAll() >> sites

        when: '/data/sites is called'
        def result = mockMvc.perform(get('/data/sites')).andReturn()
        def json = new JsonSlurper().parseText(result.response.contentAsString)

        then: 'The 5 sites should be returned'
        result.response.status == 200
        result.response.contentType == MediaType.APPLICATION_JSON_UTF8_VALUE
        json.id.size() == 5
        json.name.any { it == 'Test Site 1' }
        result.response.contentAsString
                .contains('{"id":1,"name":"Test Site 1","env":"DEV","url":"http://test-site-1.demandbridge.com","createdDate":null,"lastModifiedDate":null,"platformCode":"dbe"}')
    }
}
