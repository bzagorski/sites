package com.demandbridge.sites.site

import com.demandbridge.sites.platform.PlatformRepository
import com.demandbridge.sites.util.TestDataProvider
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.util.NestedServletException
import spock.lang.PendingFeature
import spock.lang.Specification

class SiteControllerSpec extends Specification {

    def siteRepository = Mock(SiteRepository)
    def platformRepository = Mock(PlatformRepository)
    def siteController = new SiteController(siteRepository, platformRepository)
    def mockMvc = MockMvcBuilders.standaloneSetup(siteController).build()

    def setup() {
        given:
        def sites = TestDataProvider.getTestSites().sort { [it.name] }
        siteRepository.findAllByOrderByName() >> sites
    }

    def "Index page returns site map"() {
        when:
        def result = mockMvc.perform(MockMvcRequestBuilders.get('/')).andReturn()
        def modelAndView = result.modelAndView

        then:
        def platforms = result.modelAndView.model
        platforms.containsKey('platformSiteMap')
        modelAndView.viewName == 'views/index'
    }

    def "Site detail page can be viewed"() {
        given: 'A site with id #id'
        def platform = TestDataProvider.getPlatform('dbe')
        def site = Optional.of(
                new Site(id: 1, name: 'DB Demo', url: 'https://demo.dbenterprise.com', env: Environment.PROD, platform: platform))

        when: 'Request is sent to /sites/1'
        def result = mockMvc.perform(MockMvcRequestBuilders.get('/sites/1')).andReturn()

        then: 'Site detail page should be returned'
        1 * siteRepository.findById(site.get().id) >> site
        result.modelAndView.viewName == 'views/site-detail'

        and: 'Model should contain test site'
        result.modelAndView.model.get('site') instanceof Site
    }

    def "Delete site"() {
        given:
        def platform = TestDataProvider.getPlatform('dbe')
        def site = Optional.of(
                new Site(id: 1, name: 'DB Demo', url: 'https://demo.dbenterprise.com', env: Environment.PROD, platform: platform))

        when: 'Request is sent to /sites/1/delete'
        def result = mockMvc.perform(MockMvcRequestBuilders.get('/sites/1/delete')).andReturn()

        then: 'Site should have been deleted'
        1 * siteRepository.deleteById(site.get().id)

        and: 'User should be redirected to the index'
        result.modelAndView.viewName == 'redirect:/'

        and: 'Flash Message should be displayed'
        result.flashMap.get('flashMessage') == 'Site successfully deleted'
    }

    @PendingFeature
    def "Exception thrown when Site is not found"() {
        given: 'An empty site optional is returned'
        siteRepository.findById(_) >> Optional.empty()

        when: 'A site detail page is viewed for a site that does not exist'
        mockMvc.perform(MockMvcRequestBuilders.get('/sites/5'))

        then: 'A SiteNotFoundException should be thrown'
        def ex = thrown(NestedServletException)
        ex.cause instanceof SiteNotFoundException
        ex.cause.message == 'Site with id [5] was not found'
    }
}
