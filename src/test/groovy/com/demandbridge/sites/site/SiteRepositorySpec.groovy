package com.demandbridge.sites.site

import com.demandbridge.sites.DatabaseSpecification
import spock.lang.Unroll

@Unroll
class SiteRepositorySpec extends DatabaseSpecification {

    def "Find all sites"() {
        when:
        def result = siteRepository.findAll()

        then:
        result.size() > 0
    }

    def "Find all sites by platform"() {
        when: "Site Repository is called to find all by platform"
        def platform = platformRepository.findByCode('dbe').get()
        def result = siteRepository.findAllByPlatformOrderByName(platform)

        then: "Result should only contain sites of the DBE platform"
        assert result.every { it.platform = platform }

        and: "Result should be ordered by name"
        assert result.first().name == 'DBE Site 1 Dev'
        assert result.last().name == 'DBE Site Prod'
    }

    def "Find site by URL"() {
        when: 'URL that exists for a site is provided'
        def result = siteRepository.findOneByUrl('https://dbe-site-prod.dbenterprise.com')

        then: 'Result should be dbe prod site'
        assert result.name == 'DBE Site Prod'
    }

}
