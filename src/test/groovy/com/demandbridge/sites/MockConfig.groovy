package com.demandbridge.sites

import com.demandbridge.sites.site.SiteRepository
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import spock.mock.DetachedMockFactory

@TestConfiguration
class MockConfig {

    private DetachedMockFactory factory = new DetachedMockFactory()

    @Bean
    SiteRepository siteRepository() {
        factory.Mock(SiteRepository)
    }
}
