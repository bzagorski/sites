package ui.specs

import ui.pages.IndexPage
import ui.pages.SiteAddPage
import ui.pages.SiteDetailPage

class NavSpec extends BaseUiSpec {

    def "Logo should link to the index page"() {
        given: 'User is on a page other than the index page'
        to SiteAddPage

        when: 'User clicks the nav logo'
        nav.logo.click()

        then: 'User should be on the home / index page'
        at IndexPage
    }


    def "Should be able to search for a site"() {
        when: 'User enters a query into the search box'
        to IndexPage
        nav.searchBox << 'dbe'

        then: 'Autocomplete suggestions should be visible'
        assert nav.searchSuggestionsWrapper.isDisplayed()
        assert nav.searchSuggestions.size() > 1

        and: 'User selects the first option'
        nav.searchSuggestions
                .first()
                .click()

        then: 'The site detail page for that option should be displayed'
        at SiteDetailPage
    }
}
