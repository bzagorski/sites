package ui.specs

import com.demandbridge.sites.platform.PlatformRepository
import com.demandbridge.sites.site.Environment
import com.demandbridge.sites.site.Site
import com.demandbridge.sites.site.SiteRepository
import org.springframework.beans.factory.annotation.Autowired
import ui.pages.IndexPage
import ui.pages.SiteAddPage
import ui.pages.SiteDetailPage

class SiteAddSpec extends BaseUiSpec {

    @Autowired
    SiteRepository siteRepository

    @Autowired
    PlatformRepository platformRepository

    SiteAddPage page

    def setup() {
        given: 'On the add a new site page'
        page = to SiteAddPage
    }

    def "Save a new site"() {
        when: 'A new, valid site is saved'
        Site site = new Site(
                name: 'test site 1',
                url: 'https://test1.development.dbenterprise.com',
                env: Environment.INTERNAL,
                platform: platformRepository.findByCode('itl').get())
        page.form.saveSite(site)

        then: 'The user should be directed to the index page'
        IndexPage indexPage = at IndexPage

        and: 'A flash message indicating that the site was successfully saved is displayed'
        indexPage.flashMessage.messageMatches('Site successfully added')

        and: 'Site should be able to be searched for'
        indexPage.nav.searchFor(site.name)
        at SiteDetailPage
    }

    def "Attempt to save an incomplete site"() {
        when: 'The site form is submitted without completing all fields'
        page.form.name = 'Test site'
        page.form.save()

        then: 'User should still be on the site add page'
        at SiteAddPage

        and: 'A flash message indicating that all fields need to be completed is displayed'
        page.flashMessage.messageMatches('Please ensure that all fields are completed.')
    }

    def "Attempt to save a site that already exists"() {
        given: 'A test site is saved'
        Site site = new Site(
                name: 'Test Dev Site',
                url: 'https://test.development.dbenterprise.com/',
                env: Environment.DEV,
                platform: platformRepository.findByCode('dbe').get())
        siteRepository.save(site)

        when: 'User tries to save the same site'
        page.form.saveSite(site)

        then: 'User should still be on the add a site page'
        at SiteAddPage

        and: 'A flash message indicating that the site already exists should be displayed'
        page.flashMessage.messageMatches('Site already exists!')
    }
}
