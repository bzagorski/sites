package ui.specs

import org.apache.commons.io.FileUtils
import ui.modules.Menu
import ui.pages.AddSitePage
import ui.pages.IndexPage

class MenuSpec extends BaseUiSpec {

    def testFilePath = 'src/test/resources/csv-test-export.csv'

    def cleanup() {
        def path = 'src/test/resources/csv-test-export.csv'
        def file = new File(path)
        if (file.exists()) file.delete()
    }

    def "Add and Export buttons should be visible when menu is open"() {
        given: 'On the index page'
        IndexPage page = to IndexPage

        when: 'User hovers over the floating menu button'
        interact { moveToElement(page.menu.root) }

        then: 'The add and export buttons should be visible'
        page.menu.addButton.css('opacity').toInteger() == 1
        page.menu.exportButton.css('opacity').toInteger() == 1
    }

    def "Add button should take user to Add a Site Page"() {
        given: 'On the index page'
        IndexPage page = to IndexPage

        when: 'The add a new site button is clicked'
        page.menu.clickButton(Menu.Button.ADD)

        then: 'Then the add a new site form should be visible'
        at AddSitePage
    }

    def "Export button triggers csv export"() {
        given: 'On the index page'
        IndexPage ip = to IndexPage

        when: 'A file download is mimicked'
        def connection = downloadStream(ip.menu.export.@href)
        FileUtils.copyInputStreamToFile(connection, new File(testFilePath))

        then: 'Export file should have been downloaded'
        def exportFile = new File(testFilePath)
        exportFile.exists()

        and: 'Should contain some site data'
        exportFile.readLines().any { it.contains('DBE Site 1 Dev') }
    }
}
