package ui.specs

import ui.pages.IndexPage
import ui.pages.SiteDetailPage

class IndexSpec extends BaseUiSpec {

    def setup() {
        when: 'User goes to the home page'
        to IndexPage

        then: 'The platform accordion should be visible'
        at IndexPage
    }

    def "Platform list item should expand when clicked"() {
        when: 'A platform collapsible is clicked'
        platforms.first().click()

        then: 'The sites for that platform should be visible'
        assert platformIsExpanded(0)
    }

    def "Site row should have name, url, and link to detail page"() {
        when: 'A platform collapsible is clicked'
        platforms.first().click()

        then: 'Site table for that platform should be displayed'
        assert waitFor { displayedPlatform.size() == 1 }

        and: 'User clicks the details icon for a site row'
        getDisplayedSites().first().viewDetails()

        then: 'User should be taken to the detail page for that row'
        at SiteDetailPage
    }

    def "Only one platform can be expanded at a time"() {
        when: 'A platform collapsible is clicked'
        clickPlatformByIndex(0)

        and: 'Another platform is clicked'
        sleep(500) // wait for css transition
        clickPlatformByIndex(1)

        then: 'Only one platform list item should have the active class'
        assert getExpandedPlatforms().size() == 1
        assert waitFor { !platformIsExpanded(0) }
    }
}
