package ui.specs

import com.demandbridge.sites.Application
import geb.spock.GebSpec
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import spock.lang.Ignore

@Ignore('Skip UI Tests')
@SpringBootTest(
        classes = Application,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureTestDatabase
class BaseUiSpec extends GebSpec {

    @LocalServerPort
    int port

    def setup() {
        given:
        setBaseUrl("http://localhost:${port}")
    }

}
