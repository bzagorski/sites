package ui.specs

import ui.pages.PlatformPage
import ui.pages.SiteDetailPage

class PlatformSpec extends BaseUiSpec {

    def "Sites for platform should be displayed"() {
        when: 'At DB Enterprise page'
        PlatformPage page = to PlatformPage, '1'

        then: 'DB Enterprise sites should be displayed'
        page.getDisplayedSites().size() > 0
        page.getDisplayedSites()
                .any { it.urlLink.text().contains('dbenterprise')}
    }

    def "Filter table using table filter"() {
        when: 'At DB Enterprise page'
        PlatformPage page = to PlatformPage, '1'

        and: "And I search for 'automation'"
        page.filterTable('automation')

        then: 'Only rows with automation in their name or url should be displayed'
        page.getDisplayedSites().every {
            it.urlLink.text().contains('automation') && it.name.toLowerCase().contains('automation')
        }
    }

    def "View site details for a row"() {
        when: 'At DB Enterprise page'
        PlatformPage page = to PlatformPage, '1'

        and: 'User clicks the details icon for the first site row'
        def siteName = page.getDisplayedSites().first().name
        page.getDisplayedSites().first().viewDetails()

        then: 'User should be taken to the detail page for that row'
        SiteDetailPage sdp = at SiteDetailPage
        siteName.contains(sdp.name)
    }
}
