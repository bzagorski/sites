package ui.specs

import com.demandbridge.sites.platform.PlatformRepository
import com.demandbridge.sites.site.Environment
import com.demandbridge.sites.site.Site
import com.demandbridge.sites.site.SiteRepository
import geb.Page
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Ignore
import spock.lang.IgnoreIf
import ui.pages.IndexPage
import ui.pages.SiteDetailPage


class SiteDetailSpec extends BaseUiSpec {

    Site site

    @Autowired
    SiteRepository siteRepository

    @Autowired
    PlatformRepository platformRepository

    def setup() {
        given: 'A test site is saved'
        site = new Site(
                name: 'Site for Testing',
                url: 'http://www.demandbridge.com/',
                env: Environment.INTERNAL,
                platform: platformRepository.findByCode('itl').get())
        siteRepository.save(site)

        IndexPage indexPage = to IndexPage

        and: 'The test site detail page is viewed'
        indexPage.nav.searchBox << site.name
        nav.searchSuggestions
                .first()
                .click()
    }

    def cleanup() {
        siteRepository.delete(site)
    }

    def "Site details match expected"() {
        when: 'On the test site\'s site detail page'
        SiteDetailPage page = at SiteDetailPage

        then: 'Site details match expected'
        with(page) {
            name == site.name
            details.contains(site.url)
            details.contains(site.platform.name)
            details.toUpperCase().contains(site.env.name())
        }
    }

    @IgnoreIf({ sys['geb.env']?.contains('firefox') })
    def "Go button"() {
        when:
        SiteDetailPage page = at SiteDetailPage

        and: "The Go button is clicked"
        def newUrl = withNewWindow({ page.goToSite() }) {
            return getCurrentUrl()
        }

        then: "The user is taken to the site's url in a new tab"
        assert newUrl == site.url
    }

    def "Edit button"() {
        when:
        SiteDetailPage page = at SiteDetailPage

        and: 'The edit button is clicked'
        page.bringUpEditModal()

        then: 'The edit modal should be displayed'
        page.editForm.displayed

        and: 'Displayed details should match expected'
        with(page) {
            editForm.name == site.name
            editForm.url == site.url
            editForm.env.toUpperCase() == site.env.name()
            editForm.platform == site.platform.id.toString()
        }
    }

    def "Edit site details"() {
        when:
        SiteDetailPage page = at SiteDetailPage

        and: 'User clicks the edit button'
        page.bringUpEditModal()

        and: 'The details for the site are changed'
        def newName = 'This has been edited'
        page.editForm.name = newName
        page.editForm.save()

        then: 'A flash message should be displayed indicating a successful update'
        page.flashMessage.messageMatches('Site successfully updated')

        and: 'The site details should match the edits that were made'
        page.name == newName
    }

    def "Attempt to save invalid edits"() {
        when:
        SiteDetailPage page = at SiteDetailPage

        and: 'User clicks the edit button'
        page.bringUpEditModal()

        and: 'The user attempts to save blank fields'
        page.editForm.name = ''
        page.editForm.save()

        then: 'A flash message should be displayed indicating an error occurred'
        page.flashMessage.messageMatches('Please ensure that all fields are completed.')
    }

    @Ignore("Deleting a site is disabled")
    def "Delete button"() {
        when:
        Page page = at SiteDetailPage

        and: 'The delete button is clicked'
        page.deleteSite()

        then: 'The user should be redirected to the index page'
        at IndexPage

        and: 'A successful flash message message should be displayed'
        flashMessage.root.isDisplayed()

        and: 'With message \'Site successfully deleted\''
        flashMessage.messageMatches('Site successfully deleted') == true

        and: 'Site should no longer be found'
        nav.searchBox << site.name
        nav.searchSuggestionsWrapper.displayed == false
    }
}
