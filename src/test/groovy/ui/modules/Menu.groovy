package ui.modules

import geb.Module

class Menu extends Module {

    enum Button {
        MAIN, ADD, EXPORT
    }

    static content = {
        root { $('#menu') }
        menuButton { $('#menu > a > i') }
        addButton { $('#add > i') }
        export { $('#export') }
        exportButton { $('#export > i') }
    }

    private boolean menuOpen() {
        return root.hasClass('active')
    }

    def clickButton(Button btn) {
        switch (btn) {
            case Button.MAIN:
                menuButton.click()
                break
            case Button.ADD:
                if (!menuOpen()) menuButton.click()
                addButton.click()
                break
            case Button.EXPORT:
                if (!menuOpen()) menuButton.click()
                exportButton.click()
                break
        }
    }
}
