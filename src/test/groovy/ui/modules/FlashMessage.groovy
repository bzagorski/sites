package ui.modules

import geb.Module

class FlashMessage extends Module {

    static content = {
        root { $('#flash-message') }
        message(wait: true) { root.text().trim() }
    }

    boolean messageMatches(String message) {
        return this.message == message
    }
}
