package ui.modules

import geb.Module

class SiteRow extends Module {

    static content = {
        cell { $('td') }
        name { cell[0].text() }
        urlLink { cell[1] }
        detailsLink { cell[2].find('a') }
    }

    def viewDetails() {
        detailsLink.click()
    }

    def followUrl() {
        urlLink.click()
    }

    @Override
    String toString() {
        return "siteRow[name: ${name} url: ${urlLink.text()} detailsLink: ${detailsLink.attr('href')}"
    }

}
