package ui.modules

import com.demandbridge.sites.site.Environment
import com.demandbridge.sites.site.Site
import geb.Module

class SiteForm extends Module {

    static content = {
        form { $('#siteForm') }
        submitButton { $('#save') }
        radioButton { String value -> $("input[value='${value}'] + span") }
        select { module(MaterializeSelect) }
    }

    SiteForm setName(String name) {
        form.name = name
        return this
    }

    SiteForm setUrl(String url) {
        form.url = url
        return this
    }

    SiteForm setEnv(Environment env) {
        radioButton(env.name()).click()
        return this
    }

    SiteForm setPlatform(String platformName) {
        select.selectOptionByText(platformName)
        return this
    }

    void save() {
        sleep(250) // wait for any input field transitions / animations to complete
        submitButton.click()
    }

    void saveSite(Site site) {
        this.setName(site.name)
                .setUrl(site.url)
                .setEnv(site.env)
                .setPlatform(site.platform.name)
                .save()
    }
}
