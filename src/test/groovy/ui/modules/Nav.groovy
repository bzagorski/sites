package ui.modules

import geb.Module
import ui.pages.SiteDetailPage

class Nav extends Module {

    static content = {
        logo { $('.brand-logo > img') }
        searchBox { $('#search') }
        searchSuggestionsWrapper { $('#suggestions') }
        searchSuggestions(to: SiteDetailPage) { searchSuggestionsWrapper.$('li > a') }
        navLinks { $('.nav-link') }
    }

    boolean searchFor(String query) {
        searchBox << query
        if (searchSuggestions?.size() > 0)
            return searchSuggestions.first().click()
    }

    void clickNavOption(String linkText) {
        navLinks.find(text: linkText)
                .click()
    }
}
