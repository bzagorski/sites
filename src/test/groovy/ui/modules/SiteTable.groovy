package ui.modules

import geb.Module

class SiteTable extends Module {

    static content = {
        root { $('table#sites') }
        headers { root.find('th') }
        rows { root.find('tbody > tr').moduleList(SiteRow) }
    }

}
