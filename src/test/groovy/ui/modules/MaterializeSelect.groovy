package ui.modules

import geb.Module

class MaterializeSelect extends Module {

    static content = {
        selectWrapper { $('.select-wrapper') }
        selectTrigger { selectWrapper.find('.dropdown-trigger') }
        selectList { selectWrapper.find('ul') }
        selectListItems { selectList.find('li') }
    }

    boolean isOpen() {
        return selectList.displayed
    }

    void openDropdown() {
        selectTrigger.click()
    }

    void selectOptionByText(String text) {
        if (!isOpen()) openDropdown()
        selectListItems.find(text: text).click()
    }
}
