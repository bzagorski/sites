package ui.pages

import com.demandbridge.sites.site.Site
import ui.modules.SiteForm

class SiteDetailPage extends BasePage {

    static at = { siteCard }

    static content = {
        siteCard { $('#site-details-wrapper') }
        name { siteCard.find('.card-title').text() }
        details { $('#site-details').text() }
        goButton { $('#site-go') }
        editButton { $('#site-edit') }
        deleteButton(to: IndexPage) { $('#site-delete') }
        editForm(wait: true) { $('#edit-site').module(SiteForm) }
    }

    def goToSite() {
        goButton.click()
    }

    def bringUpEditModal() {
        editButton.click()
    }

    def editSite(Site site) {
        editForm.setName(site.name)
                .setUrl(site.url)
                .setEnv(site.env)
                .setPlatform(site.platform.id)
                .save()
    }

    @Deprecated // Deleting a site is disabled
    def deleteSite() {
        deleteButton.click()
    }
}