package ui.pages

import geb.Page
import ui.modules.FlashMessage
import ui.modules.Menu
import ui.modules.Nav

class BasePage extends Page {

    static content = {
        nav { module(Nav) }
        menu { module(Menu) }
        flashMessage { module(FlashMessage) }
    }
}
