package ui.pages


import ui.modules.SiteTable

class IndexPage extends BasePage {

    static url = '/'

    static at = { platformContainter }

    static content = {
        platformContainter { $('#platforms') }
        platformWrappers { $('.platform-collapsible') }
        displayedPlatform { platformWrappers.findAll { it.hasClass('active') } }
        platforms { $('.platform-collapsible > .collapsible-header') }
    }

    def platformIsExpanded(int index) {
        platformWrappers.getAt(index)
                .hasClass('active')

    }

    def clickPlatform(String platformName) {
        platforms.find(text: platformName)
                .click()
    }

    def clickPlatformByIndex(int index) {
        platforms.getAt(index)
                .click()
    }

    def getExpandedPlatforms() {
        return platformWrappers.findAll { it.hasClass('active') }
    }

    def getDisplayedSites() {
        return displayedPlatform.module(SiteTable)
                .rows
    }
}
