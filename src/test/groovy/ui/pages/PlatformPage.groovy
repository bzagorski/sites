package ui.pages

import ui.modules.SiteRow
import ui.modules.SiteTable

class PlatformPage extends BasePage {

    static url = '/platforms'

    String convertToPath(String id) {
        "/$id/sites"
    }

    static at = { tableFilter }

    static content = {
        logo { $('#platform-logo') }
        tableFilter { $('#filter') }
        filterTable { String query -> tableFilter << query }
        table { module(SiteTable) }
    }

    List<SiteRow> getDisplayedSites() {
        return table.rows.findAll { it.displayed }
    }
}
