package ui.pages


class AddSitePage extends BasePage {

    static url = '/sites/add'

    static at = { siteForm }

    static content = {
        siteForm { $('#siteForm') }
    }

}
