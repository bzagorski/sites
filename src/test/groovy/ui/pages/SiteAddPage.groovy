package ui.pages

import ui.modules.FlashMessage
import ui.modules.SiteForm

class SiteAddPage extends BasePage {

    static url = '/sites/add'

    static at = { wrapper }

    static content = {
        wrapper { $('.site-add-wrapper') }
        form { module(SiteForm) }
        flashMessage { module(FlashMessage) }
    }
}
