import io.github.bonigarcia.wdm.ChromeDriverManager
import io.github.bonigarcia.wdm.FirefoxDriverManager
import org.openqa.selenium.Dimension
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions

waiting {
    timeout = 5
}

environments {
    chrome {
        ChromeDriverManager.getInstance().setup()
        driver = {
            ChromeDriver driverInstance = new ChromeDriver()
            driverInstance.manage().window().setSize(new Dimension(1440, 900))
            driverInstance
        }
    }

    chromeHeadless {
        ChromeDriverManager.getInstance().setup()
        ChromeOptions options = new ChromeOptions()
        options.addArguments("disable-gpu", "headless", "window-size=1440,900")
        driver = { new ChromeDriver(options) }
    }

    firefox {
        atChecking = 10
        driver = {
            FirefoxOptions options = new FirefoxOptions()
            options.addPreference('security.insecure_password.ui.enabled', false)
            options.addPreference('security.insecure_field_warning.contextual.enabled', false)
            FirefoxDriverManager.getInstance().setup()
            new FirefoxDriver(options)
        }
    }
}

driver = {
    ChromeDriverManager.getInstance().setup()
    ChromeDriver driverInstance = new ChromeDriver()
    driverInstance.manage().window().setSize(new Dimension(1440, 900))
    driverInstance
}

reportsDir = "geb-reports"
