ALTER SEQUENCE IF EXISTS SITE_LIBRARY_SITE_SEQ RESTART WITH 12;
ALTER SEQUENCE IF EXISTS SITE_LIBRARY_PLATFORM_SEQ RESTART WITH 6;

Insert into SITE_LIBRARY_PLATFORM (ID,NAME,CODE) values (1,'DB Enterprise','dbe');
Insert into SITE_LIBRARY_PLATFORM (ID,NAME,CODE) values (2,'DB Alliance','dba');
Insert into SITE_LIBRARY_PLATFORM (ID,NAME,CODE) values (3,'Pay App','pay');
Insert into SITE_LIBRARY_PLATFORM (ID,NAME,CODE) values (4,'Campaign Manager','cm');
Insert into SITE_LIBRARY_PLATFORM (ID,NAME,CODE) values (5,'Internal','itl');

Insert into site_library_site (ID,NAME,ENV,URL,PLATFORM_ID, CREATED_DATE) values (1,'DBE Site 1 Dev','DEV','https://dbe-site-dev-1.development.dbenterprise.com',1, CURRENT_TIMESTAMP);
Insert into site_library_site (ID,NAME,ENV,URL,PLATFORM_ID, CREATED_DATE) values (10,'DBE Site 2 Dev','DEV','https://dbe-site-dev-2.development.dbenterprise.com',1, CURRENT_TIMESTAMP);
Insert into site_library_site (ID,NAME,ENV,URL,PLATFORM_ID, CREATED_DATE) values (11,'DBE Site 3 Dev','DEV','https://dbe-site-dev-3.development.dbenterprise.com',1, CURRENT_TIMESTAMP);
Insert into site_library_site (ID,NAME,ENV,URL,PLATFORM_ID, CREATED_DATE) values (2,'DBE Site Prod','PROD','https://dbe-site-prod.dbenterprise.com',1, CURRENT_TIMESTAMP);
Insert into site_library_site (ID,NAME,ENV,URL,PLATFORM_ID, CREATED_DATE) values (3,'DBA Site Dev','DEV','https://dba-site-dev.dba.development.dbenterprise.com/',2, CURRENT_TIMESTAMP);
Insert into site_library_site (ID,NAME,ENV,URL,PLATFORM_ID, CREATED_DATE) values (4,'DBE Site Prod','PROD','https://dba-site-dev.dba.demandbridge.com/',2, CURRENT_TIMESTAMP);
Insert into site_library_site (ID,NAME,ENV,URL,PLATFORM_ID, CREATED_DATE) values (5,'PAY Site Dev','DEV','https://pay-site-dev.development.dbenterprise.com/pay',3, CURRENT_TIMESTAMP);
Insert into site_library_site (ID,NAME,ENV,URL,PLATFORM_ID, CREATED_DATE) values (6,'PAY Site Prod','PROD','https://pay-site-prod.dba.demandbridge.com/pay',3, CURRENT_TIMESTAMP);
Insert into site_library_site (ID,NAME,ENV,URL,PLATFORM_ID, CREATED_DATE) values (7,'CM Site Dev','DEV','https://cm-site-dev.development.dbenterprise.com/cm',4, CURRENT_TIMESTAMP);
Insert into site_library_site (ID,NAME,ENV,URL,PLATFORM_ID, CREATED_DATE) values (8,'CM Site Prod','DEV','https://cm-site-prod.dbenterprise.com/cm',4, CURRENT_TIMESTAMP);
Insert into site_library_site (ID,NAME,ENV,URL,PLATFORM_ID, CREATED_DATE) values (9,'Internal Site','INTERNAL','https://service.demandbridge.com',5, CURRENT_TIMESTAMP);