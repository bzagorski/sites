# Site Library

### Engineering
* Used Groovy (Don't freak out it's Java with lots of niceties :)  )
    - Some cool stuff:
        + Classes marked with ```java @CompileStatic``` will be statically compiled to Java bytecode rather than using dynamic dispatch
            * This leads to similar or better performance as writing it in Java
        + Collections have default methods similar to streams
        
```groovy
def list = ['one', 'two', 'three']
def one = list.collect { listItem -> listItem == 'one' }
def oneShortened = list.collect { it == 'one' }
def uppercase = list.each { it.toUpperCase() }
def assertAllStrings = list.every { it instanceof String }
```

* Spock Framwork was used for tests and it's very declaritive and has good reporting

### Why
* Central location for development and productions sites
* Searchable interface
* Contains dev / live sites across db platforms as sites for internal use
* New sites can be added if you didn't find the one you're looking for
* Note that I have not pre-loaded this with a comprehensive list of sites, mostly what I had bookmarked and what was on the DB Admin links html page
    * Feel free to add anything that you do not see
    * If you see something that is no longer used, let me know and I can remove it or add 'deprecated - ' to the name

### How
* Groovy + Spring Boot + Freemarker
* Check out model classes (com.demandbridge.site.Site) and the controllers to see some of the stuff you get for free
    * Groovy automatically compiles getters and setters for class properties (like lombok)
    * New objects can be instantiated using properties in their constructors, rather than creating a constructor for every possible combination
    ```
    User newUser = new User(firstName: 'First', lastName: 'Last', username: 'user1')
    ```
        * Properties that are not used in the constructor will automatically be null
    * Test folder shows off Spock Test Framework which is a declaritive given, when, then style unit testing framework built on JUnit

##### Common
* Click Search icon to enter a type ahead or keyword search
    * URL and Name are the best search parameters
    * Submitting without selected a typeahead result will take you to a results page, displaying any sites that matched your search

* Use Click platform nav option to view sites for just that platform
* Click logo to return to homepage
* Hover over the floating button in the lower right corner to bring up the export all and add site buttons
    * Export All
        * This will generate a .csv of all sites
    * Add
        * Add a new site

##### Home Page
* Contains all, current, platforms and internal collapsibles
    * Click to expand list of sites separated by Dev / Prod
* Click the URL link to go directly to that site's location
* Click the Details icon to view the detail page for that site

##### Platform Page
* Contains table list of sites specific to selected platform
* Table layout is the same as what is on the home page
* Sites on the page can be filtered down using the Table Filter text field on the right

##### Site Overview Page
* Contains basic information for selected site
* Click the GO link to go to the site
* Click EDIT to bring up the Edit modal
* Click the platform logo of the overview to go to that platform page

##### Add New Site Page
* Create a new site
* The URL must be unique, if a site entry already exists with the same URL then you will not be able to add a new one
* Name: The name of the site
* Url: The site URL
* Environment: The environment appropriate for the site. Development, Production, or N/A for internal sites
* Platform: The platform the site is tied to. Internal is used for any site not tied to a db platform, such as JIRA. These may be expanded in the future after integration with EQ and KS
